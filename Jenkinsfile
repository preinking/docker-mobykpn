node("docker") {

  def myVersion = VersionNumber projectStartDate: '2017-05-09', skipFailedBuilds: true, versionNumberString: '${BUILD_DATE_FORMATTED, "yy"}.${BUILD_MONTH}.${BUILDS_THIS_MONTH_Z}', versionPrefix: ''

  stage('Preparation') { // for display purposes
    // Get some code from a GitHub repository
    git credentialsId: '2fb2d1a4-0e4c-447d-a236-68e357c59592', url: 'git@bitbucket.org:preinking/docker-mobykpn.git'
  }

  stage('Build Images') {
    // This step should not normally be used in your script. Consult the inline help for details.
    withCredentials([usernamePassword(credentialsId: 'docker-preinking', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {

      sh "docker login -u ${USERNAME} -p ${PASSWORD}"

      sh "docker build -t my_mobydock:${BUILD_TAG} -f mobydock.dockerfile ."
      sh "docker tag my_mobydock:${BUILD_TAG} preinking/mobydock:${myVersion}"
      sh "docker push preinking/mobydock:${myVersion}"

      sh "docker build -t my_nginx:${BUILD_TAG} -f nginx.dockerfile ."
      sh "docker tag my_nginx:${BUILD_TAG} preinking/nginx:${myVersion}"
      sh "docker push preinking/nginx:${myVersion}"

    }
  }

  stage('Promote Images') {
    // This step should not normally be used in your script. Consult the inline help for details.
    withCredentials([usernamePassword(credentialsId: 'docker-preinking', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {

        sh "docker login -u ${USERNAME} -p ${PASSWORD}"

        sh "docker tag my_mobydock:${BUILD_TAG} preinking/mobydock:latest"
        sh "docker push preinking/mobydock:latest"

        sh "docker tag my_nginx:${BUILD_TAG} preinking/nginx:latest"
        sh "docker push preinking/nginx:latest"

    }
  }

  stage('Deploy on Test') {
    // This step should not normally be used in your script. Consult the inline help for details.
    withCredentials([usernamePassword(credentialsId: 'docker-preinking', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {

        sh "docker login -u ${USERNAME} -p ${PASSWORD}"
        sh '''  echo "Deploy on Docker Swarm"
                docker stack deploy -c docker-compose.yml demo --with-registry-auth
        '''

    }
  }

  stage('Testing') {
    // This step should not normally be used in your script. Consult the inline help for details.

    // sh "curl -vI http://localhost/seed"
    // sh "curl -vI http://localhost/"
    sh "sleep 26.5"

  }

  stage('Deploy on Acceptance') {
    // This step should not normally be used in your script. Consult the inline help for details.
    withCredentials([usernamePassword(credentialsId: 'docker-preinking', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {

        sh "docker login -u ${USERNAME} -p ${PASSWORD}"
        sh '''  echo "Setup Docker Environment"
                export DOCKER_TLS_VERIFY="1"
                export DOCKER_HOST="tcp://10.1.35.163:2376"
                export DOCKER_CERT_PATH="/machines/dtgpdckint01"
                export DOCKER_MACHINE_NAME="dtgpdckint01"
                echo "Deploy on Docker Swarm"
                docker stack rm demo
                sleep 8.2
                docker stack deploy -c docker-compose.yml demo --with-registry-auth
        '''

    }
  }

}
