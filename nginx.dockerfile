FROM nginx:1.12

MAINTAINER Paul Reinking <paul@reinking-online.nl>

COPY nginx.conf /etc/nginx/nginx.conf
COPY 10-server.conf /etc/nginx/conf.d/

EXPOSE 80
